// We require the Hardhat Runtime Environment explicitly here. This is optional
// but useful for running the script in a standalone fashion through `node <script>`.
//
// When running the script with `npx hardhat run <script>` you'll find the Hardhat
// Runtime Environment's members available in the global scope.
const hre = require("hardhat");


async function main() {
  // Hardhat always runs the compile task when running scripts with its command
  // line interface.
  //
  // If this script is run directly using `node` you may want to call compile
  // manually to make sure everything is compiled
  // await hre.run('compile');

  // We get the contract to deploy
  const [owner] = await ethers.getSigners();
  const Token = await ethers.getContractFactory("Marvel1155")
  const token = await Token.attach("0x47B9931B59Ced09F50F8a55da428B6F5E8D2AF0c")
  //const Greeter = await hre.ethers.getContractFactory("MyToken");
 // const greeter = await Greeter.deploy();
 const balance = await token.balanceOf("0x0409aBe1679D41A1c8B5D777e763a54fa1667403","0");
 //token.awardItem("0x3d63e5809F3A70E08D082f66fa1cb91B4d5D49c1","https://api.frank.hk/api/nft/demo/1155/marvel/2.json");

 // await greeter.deployed();

  console.log("Greeter deployed to:", balance );//greeter.address);
}

// We recommend this pattern to be able to use async/await everywhere
// and properly handle errors.
main()
  .then(() => process.exit(0))
  .catch((error) => {
    console.error(error);
    process.exit(1);
  });
