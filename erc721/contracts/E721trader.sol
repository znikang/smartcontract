// SPDX-License-Identifier: MIT
pragma solidity ^0.8.0;

import "@openzeppelin/contracts/token/ERC721/ERC721.sol";


contract Ownable {
  address public owner;


  event OwnershipRenounced(address indexed previousOwner);
  event OwnershipTransferred(
    address indexed previousOwner,
    address indexed newOwner
  );


  /**
   * @dev The Ownable constructor sets the original `owner` of the contract to the sender
   * account.
   */
    constructor()  {
    owner = msg.sender;
  }

  /**
   * @dev Throws if called by any account other than the owner.
   */
  modifier onlyOwner() {
    require(msg.sender == owner);
    _;
  }

  /**
   * @dev Allows the current owner to transfer control of the contract to a newOwner.
   * @param newOwner The address to transfer ownership to.
   */
  function transferOwnership(address newOwner) public onlyOwner {
    require(newOwner != address(0));
    emit OwnershipTransferred(owner, newOwner);
    owner = newOwner;
  }

  /**
   * @dev Allows the current owner to relinquish control of the contract.
   */
  function renounceOwnership() public onlyOwner {
    emit OwnershipRenounced(owner);
    owner = address(0);
  }
}

contract ReentrancyGuarded {

    bool reentrancyLock = false;

    /* Prevent a contract function from being reentrant-called. */
    modifier reentrancyGuard {
        if (reentrancyLock) {
            revert();
        }
        reentrancyLock = true;
        _;
        reentrancyLock = false;
    }

}

contract ERC721trader  is ReentrancyGuarded  , Ownable {

     uint256 public x;

     uint256 public y;
     
     function setx(uint256 _x)  public  {
        x = _x;
    }

    mapping (address => mapping(uint256 => Listing)) public listings;
    mapping (address => uint256) public balacnes;
    struct Listing{
        uint256 price;
        uint256 tokenid;
        address seller;
        address tokencontract;

    }

    function addCell (uint256 price , address contractAdr , uint256 tokenid) public payable{
        
        ERC721 ct = ERC721(contractAdr); 
        require(ct.ownerOf(tokenid) == msg.sender," did you ownd the token ?");
        require(ct.isApprovedForAll(msg.sender,address(this))," contract mest ve approved ");

        listings[contractAdr][tokenid] = Listing (price,tokenid,msg.sender, contractAdr);
    }

    function buyit(address contractAdr ,address payable seller , uint256 tokenid )public payable {
        Listing memory item = listings[contractAdr][tokenid];
        ERC721 ct = ERC721(contractAdr); 
        require(msg.value >= item.price , "ifsufficeient funds sent");
        buyit_( ct, item, seller);
    }

     function buyit_(ERC721  token,  Listing memory  item ,address payable seller)
        internal
        reentrancyGuard{

          
            token.safeTransferFrom(item.seller, msg.sender, item.tokenid);    
            delete listings[item.tokencontract][item.tokenid];
            seller.transfer(item.price);

        }

    function withdraw(uint256 price , address payable destAddre) public {
        require (price <= balacnes[msg.sender]," insufficent funds");

         destAddre.transfer(price);
         balacnes[msg.sender] -= price;
    }


}



contract ZExchange is ERC721trader {

   constructor ()  Ownable() {
       
    }
}
