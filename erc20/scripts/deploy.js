async function main () {
  // We get the contract to deploy
  const Box = await ethers.getContractFactory('CroToken');
  console.log('Deploying Box...');
  const box = await Box.deploy('ZXZ','ZXZ',100000000,5,true,'0x0409aBe1679D41A1c8B5D777e763a54fa1667403','0x91D71C0F3892E084b068EE476BC924a58059494E','0xAF9C96E83874Df1e0CB765254A3409B302F41414','0x2540817b14A25C0f5A30c086e15E2f474Afa04B8','0x3d63e5809F3A70E08D082f66fa1cb91B4d5D49c1');
  await box.deployed();
  console.log('Box deployed to:', box.address);
}

main()
  .then(() => process.exit(0))
  .catch(error => {
    console.error(error);
    process.exit(1);
  });
